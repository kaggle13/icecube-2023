# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 10/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
import logging
from typing import Any, Union, List, Tuple, Optional
from pathlib import Path
import numpy as np
import torch
from torch import nn
from enum import Enum
import sys
import os
import re

logging.basicConfig(level=logging.DEBUG)


def get_logger(name):
    # log = logging.getLogger(name)
    # log.setLevel(logging.DEBUG)
    class _Logger:
        def __init__(self, name):
            self.name = name

        def __getattr__(self, attr):
            if attr.lower() in {'debug', 'info', 'warn', 'warning', 'error', 'critical'}:
                def _log(*args):
                    return print(f'{self.name} {attr.upper()}', *args, file=sys.stderr)

                return _log

    return _Logger(name)


def model_size(model) -> (int, int):
    counts = [(p.numel(), p.requires_grad) for p in model.parameters()]
    return sum(x for x, grad in counts if grad), sum(x for x, _ in counts)


MAX_TRAIN_EVENT_ID = 2147483627
TOTAL_TRAIN_SAMPLES = 21692566336
MAX_TRAIN_BATCH_ID = 660
EVAL_START_BATCH_ID = 660  # 600

MAX_TEST_BATCH_ID = 661

NUM_SENSORS = 5160
COLUMN_ORDER = ['time', 'charge', 'auxiliary', 'x', 'y', 'z']
PAD = (-1, 0, 1, 0, 0, 0)
END = (-1, 0, 1, -600, -600, -600)
CONTINUE = (-1, 0, 1, 600, 600, 600)
DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
BATCH_SIZE=32

def find_next_model_version(model_name: str, path: Path) -> int:
    import re
    path = Path(path)
    try:
        vers = int(
            re.search(f'{model_name}\\.(\\d+)', str(max(list(path.glob(f'{model_name}.*.pt'))))).group(1)
        )
        print(f'[find_model_verson] Found up to version {model_name}.{vers:03d}.pt')
        return vers + 1
    except:
        return 1


def load(path: Union[str, Path], device=DEVICE) -> (Any, torch.nn.Module, float):
    x = torch.load(path, map_location=device)
    model: torch.nn.Module = x['model_class'](x['cfg'])
    model.load_state_dict(x['model_state_dict'])
    print(f'[load] Loaded model {path}')
    return x['cfg'], model, x['val_loss']


def save(directory: Union[str, Path], cfg: Any, model: torch.nn.Module, vers: int = 1, val_loss: float = float('inf')):
    directory = Path(directory)
    directory.mkdir(exist_ok=True)
    fname = directory / f'{model.name}.{vers:03d}.pt'
    torch.save(
        {
            'cfg': cfg,
            'model_state_dict': model.state_dict(),
            'model_class': model.__class__,
            'val_loss': val_loss
        },
        fname
    )
    print(f'Saved to {fname}')


def create_submission(event_ids: np.ndarray, outputs: np.ndarray, path: Union[str, Path]):
    path = Path(path)

    norm = np.linalg.norm(outputs.astype(float), axis=1)

    outputs = pd.DataFrame({
        'event_id': event_ids,
        'x': outputs[:, 0] / norm,
        'y': outputs[:, 1] / norm,
        'z': outputs[:, 2] / norm,
    })
    outputs = outputs.groupby('event_id').agg('mean').reset_index()
    outputs['azimuth'] = np.pi / 2 - np.arctan(outputs['x'] / outputs['y'])
    outputs['azimuth'] = outputs['azimuth'].mask(outputs['y'] < 0, outputs['azimuth'] + np.pi)
    outputs['zenith'] = np.arccos(outputs['z'])

    path.parent.mkdir(exist_ok=True)
    outputs.drop(columns=['x', 'y', 'z']).to_csv(path, index=False)
    return outputs


"""
Create a dataloader that presents data as samples from a (truncated) time series
"""

from typing import Generator, List, Tuple, Optional, Union

import numpy as np
import pandas as pd
from torch import FloatTensor, LongTensor
from torch.utils.data import IterableDataset, get_worker_info, DataLoader

log = get_logger('dataloaders')


def read_meta(
        name: Union[str, Path], part_id: int,
        columns: List[str] = ('event_id', 'azimuth', 'zenith'),
        low_memory: bool = True,
        cartesian_coords: bool = True,
) -> pd.DataFrame:
    df = pd.read_parquet(name, columns=list(columns) + ['batch_id', ]).reset_index()
    df = df[df['batch_id'] == part_id].drop(columns=['batch_id', ])

    if all(x in df.columns for x in ['azimuth', 'zenith']):
        df = df.astype({'azimuth': np.float32, 'zenith': np.float32})

        if cartesian_coords:
            df['tx'] = (df['azimuth'].apply(np.cos) * df['zenith'].apply(np.sin)).astype(np.float32)
            df['ty'] = (df['azimuth'].apply(np.sin) * df['zenith'].apply(np.sin)).astype(np.float32)
            df['tz'] = (df['zenith'].apply(np.cos)).astype(np.float32)
            df = df.drop(columns=['azimuth', 'zenith'])

    return df


def read_part(name: Union[str, Path]) -> pd.DataFrame:
    df = pd.read_parquet(name).reset_index()
    df = df.astype({
        'charge': np.float32,
        'sensor_id': int,
        'auxiliary': np.float32,
    })

    df['charge'] = np.log10(df['charge'])
    # df['time'] = np.log10(df['time'])
    df['auxiliary'] -= 0.5
    return df


class Mode(Enum):
    train = 'train'
    test = 'test'
    eval = 'eval'


def sample_generator(
        base_path: Union[str, Path],
        part_id: int,
        seq_len: int,
        mode: Mode = Mode.train
) -> Generator[List[Tuple[float, float, float, float, float, float]], None, None]:
    base_path = Path(base_path)
    meta = read_meta(base_path / f'{"test" if mode is Mode.test else "train"}_meta.parquet',
                     part_id,
                     columns=['event_id', 'first_pulse_index', 'last_pulse_index', 'azimuth',
                              'zenith'] if mode is Mode.train else ['event_id', 'first_pulse_index',
                                                                    'last_pulse_index', ],
                     cartesian_coords=mode is Mode.train)

    part = read_part(base_path / ('test' if mode is Mode.test else 'train') / f'batch_{part_id}.parquet')
    part = part.merge(meta, on='event_id', how='left')
    if mode is Mode.train:
        part = part[[
            'event_id', 'sensor_id', 'time', 'charge', 'auxiliary', 'tx', 'ty', 'tz'
        ]]
    else:
        part = part[[
            'event_id', 'sensor_id', 'time', 'charge', 'auxiliary'
        ]]

    meta = meta[['event_id', 'first_pulse_index', 'last_pulse_index', ]].to_numpy()
    part = part.to_numpy()
    zeros = lambda: np.zeros((NUM_SENSORS, 3), dtype=np.float32)
    for event_id, start, end in meta:
        event_id = int(event_id)
        end += 1
        relend = min(end - start, NUM_SENSORS)
        # print(f'sample_generator event={event_id} relend={relend}')
        features = zeros()
        group = part[start:start + relend]
        sensor_ids = group[:, 1]
        time = group[:, 2]
        time = time - np.roll(time, 1)  # convert to delta time
        time[time <= 0] = 1
        group[:, 2] = np.log10(time)
        features[sensor_ids.astype(int), :] = group[:, 2:5]
        target = group[0, 5:8] if mode is Mode.train else None
        yield features, event_id, target


class IceCubeDataset(IterableDataset):

    def __init__(self, base_path: Union[str, Path], sequence_length: int = 256, mode: Mode = Mode.train):
        self.base_path = Path(base_path)
        self.mode = mode
        self.seq_len = max(16, sequence_length or 0)
        self._parts = None
        self.curr_part = -1

    @property
    def assigned_parts(self):
        if not self._parts:
            self._parts = self.get_part_ids()
        return self._parts

    def get_part_ids(self):
        if self.mode is Mode.train:
            info = get_worker_info()
            if info:
                _id = info.id
                n = info.num_workers
                m = EVAL_START_BATCH_ID // n
            else:
                _id = 0
                n = 1
                m = 1

            part_ids = list(range(1, EVAL_START_BATCH_ID + 1))

            if _id != n - 1:
                parts = part_ids[m * _id: m * (_id + 1)]
            else:
                parts = part_ids[m * _id:]

            if n > 1:
                log.info(f'Worker {_id}/{n - 1}: targeted parts={parts}')
            else:
                assert parts == part_ids, 'Somehow not all batches are being targeted.'
            return parts[:100]
        elif self.mode is Mode.eval:
            assert get_worker_info() is None, "Multiprocessing is not allowed while evaluating"
            parts = list(range(EVAL_START_BATCH_ID, MAX_TRAIN_BATCH_ID + 1))
            log.info(f'Main worker: evaluating mode - targeted parts={parts}')
            return parts
        else:
            assert get_worker_info() is None, "Multiprocessing is not allowed while testing"
            parts = pd.read_parquet(
                '/kaggle/input/icecube-neutrinos-in-deep-ice/test_meta.parquet',
                columns=['batch_id'],
            )['batch_id'].tolist()
            log.info(f'Main worker: testing mode - targeted parts={parts}')
            return parts

    def __iter__(self):
        for p in self.assigned_parts:
            self.curr_part = p
            yield from sample_generator(
                base_path=self.base_path,
                part_id=p,
                seq_len=self.seq_len,
                mode=self.mode,
            )


def collate_fn(batch: List[Tuple[list, int, tuple]]):
    features = FloatTensor(np.stack([x for x, _, _ in batch], axis=0))
    eids = LongTensor([eid for _, eid, _ in batch])
    if batch[0][2] is None:
        out = features, eids
    else:
        out = features, eids, FloatTensor(np.stack([tgt for _, _, tgt in batch], axis=0))
    return out


def create_dataloader(
        base_path: Union[str, Path],
        sequence_length: int = 256,
        batch_size: int = 2048,
        mode: Mode = Mode.train,
        num_workers: Optional[int] = None
) -> DataLoader:
    base_path = Path(base_path)
    log.info(f'Each batch weights {batch_size * NUM_SENSORS * 3 * 4 * 8 / 1e6:.3f}Mb')
    ds = IceCubeDataset(base_path, sequence_length=sequence_length, mode=mode)
    dl = DataLoader(
        ds,
        batch_size=batch_size,
        num_workers=(num_workers or 0) if mode is not Mode.test else 0,
        pin_memory=True,
        collate_fn=collate_fn,
        drop_last=mode is Mode.train,
    )

    def estimate_len():
        if mode is Mode.train:
            end = pd.read_parquet(base_path / 'train_meta.parquet', columns=['batch_id', 'last_pulse_index'])
            end = end[end['batch_id'] < EVAL_START_BATCH_ID]
        elif mode is Mode.eval:
            end = pd.read_parquet(base_path / 'train_meta.parquet', columns=['batch_id', 'last_pulse_index'])
            end = end[(end['batch_id'] >= EVAL_START_BATCH_ID) & (end['batch_id'] <= MAX_TRAIN_BATCH_ID)]
        elif mode is Mode.test:
            end = pd.read_parquet(base_path / 'test_meta.parquet', columns=['last_pulse_index'])
        else:
            raise

        return len(end) // BATCH_SIZE if mode is Mode.train else len(end) // BATCH_SIZE + int(
            len(end) % BATCH_SIZE != 0)

    dl.estimated_len = estimate_len()
    return dl


if __name__ == '__main__':
    dl = create_dataloader('dataset', sequence_length=256, batch_size=BATCH_SIZE, mode=Mode.train)
    for i, x in enumerate(dl):
        print(
            x[0].shape,
            x[1].shape,
            x[2].shape,
        )
        if i > 3:
            break
