# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 04/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from typing import Tuple

import numpy as np


def spherical2cartesian(
        azimuth: float | np.ndarray, zenith: float | np.ndarray, radius: float | np.ndarray = 1
) -> Tuple[float, ...] | Tuple[np.ndarray, ...]:
    x = radius * np.cos(azimuth) * np.sin(zenith)
    y = radius * np.sin(azimuth) * np.sin(zenith)
    z = radius * np.cos(zenith)
    return x, y, z


def cartesian2spherical(
        x: float | np.ndarray, y: float | np.ndarray, z: float | np.ndarray
) -> Tuple[float, ...] | Tuple[np.ndarray, ...]:
    radius = np.sqrt(np.square(x) * np.square(y) + np.square(z))
    azimuth = np.arctan(y / x)
    azimuth[(x > 0) & (y < 0)] += 2 * np.pi
    azimuth[(x < 0) & (y > 0)] += np.pi
    azimuth[(x < 0) & (y < 0)] += np.pi
    zenith = np.arctan(np.sqrt(np.square(x) + np.square(y)) / z)
    # zenith[(z > 0) & (y < 0)] += 2 * np.pi
    zenith[(z < 0)] += np.pi
    # zenith[(z < 0) & (y < 0)] += np.pi
    return azimuth, zenith, radius


def angular_dist_score(az_true, zen_true, az_pred, zen_pred):
    '''
    calculate the MAE of the angular distance between two directions.
    The two vectors are first converted to cartesian unit vectors,
    and then their scalar product is computed, which is equal to
    the cosine of the angle between the two vectors. The inverse
    cosine (arccos) thereof is then the angle between the two input vectors

    Parameters:
    -----------

    az_true : float (or array thereof)
        true azimuth value(s) in radian
    zen_true : float (or array thereof)
        true zenith value(s) in radian
    az_pred : float (or array thereof)
        predicted azimuth value(s) in radian
    zen_pred : float (or array thereof)
        predicted zenith value(s) in radian

    Returns:
    --------

    dist : float
        mean over the angular distance(s) in radian
    '''

    if not (np.all(np.isfinite(az_true)) and
            np.all(np.isfinite(zen_true)) and
            np.all(np.isfinite(az_pred)) and
            np.all(np.isfinite(zen_pred))):
        raise ValueError("All arguments must be finite")

    # pre-compute all sine and cosine values
    sa1 = np.sin(az_true)
    ca1 = np.cos(az_true)
    sz1 = np.sin(zen_true)
    cz1 = np.cos(zen_true)

    sa2 = np.sin(az_pred)
    ca2 = np.cos(az_pred)
    sz2 = np.sin(zen_pred)
    cz2 = np.cos(zen_pred)

    # scalar product of the two cartesian vectors (x = sz*ca, y = sz*sa, z = cz)
    scalar_prod = sz1 * sz2 * (ca1 * ca2 + sa1 * sa2) + (cz1 * cz2)

    # scalar product of two unit vectors is always between -1 and 1, this is against nummerical instability
    # that might otherwise occure from the finite precision of the sine and cosine functions
    scalar_prod = np.clip(scalar_prod, -1, 1)

    # convert back to an angle (in radian)
    return np.average(np.abs(np.arccos(scalar_prod)))