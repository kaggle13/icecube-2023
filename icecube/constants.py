# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 09/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------

MAX_TRAIN_EVENT_ID = 2147483627
TOTAL_TRAIN_SAMPLES = 21692566336
MAX_TRAIN_BATCH_ID = 660

MAX_TEST_BATCH_ID = 661

COLUMN_ORDER = ['time', 'charge', 'auxiliary', 'x', 'y', 'z']
PAD = (-1, 0, 1, 0, 0, 0)
END = (-1, 0, 1, -600, -600, -600)
CONTINUE = (-1, 0, 1, 600, 600, 600)
