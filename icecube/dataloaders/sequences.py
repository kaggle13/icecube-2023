# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 07/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
"""
Create a dataloader that presents data as samples from a (truncated) time series
"""
from enum import Enum
from pathlib import Path
from typing import Generator, List, Tuple, Optional, Union

import numpy as np
import polars as pl
from polars import col
from torch import FloatTensor
from torch.utils.data import IterableDataset, get_worker_info, DataLoader

from icecube.constants import PAD, MAX_TRAIN_BATCH_ID, MAX_TEST_BATCH_ID
from icecube.logging import get_logger

log = get_logger(__name__)


def read_meta(
        name: Union[str, Path], part_id: int,
        columns: List[str] = ('event_id', 'azimuth', 'zenith'),
        low_memory: bool = True,
        cartesian_coords: bool = True,
) -> pl.DataFrame:
    df = pl.scan_parquet(name, low_memory=low_memory).filter(col('batch_id') == part_id)

    columns = [c for c in columns if c in df.columns]
    if columns:
        df = df.select(columns)

    if all(x in df.columns for x in ['azimuth', 'zenith']):
        df = df.with_columns([
            col('azimuth').cast(pl.Float32),
            col('zenith').cast(pl.Float32),
        ])

        if cartesian_coords:
            df = df.with_columns([
                (col('azimuth').apply(np.cos) * col('zenith').apply(np.sin)).cast(pl.Float32).alias('tx'),
                (col('azimuth').apply(np.sin) * col('zenith').apply(np.sin)).cast(pl.Float32).alias('ty'),
                (col('zenith').apply(np.cos)).cast(pl.Float32).alias('tz'),
            ]).drop(['azimuth', 'zenith'])

    return df.collect()


def read_part(name: Union[str, Path]) -> pl.DataFrame:
    df = pl.scan_parquet(name).with_columns([
        col('charge').apply(np.log10).cast(pl.Float32),
        col('sensor_id').cast(pl.Int16),
        col('auxiliary').cast(pl.Float32),
    ])
    return df.collect()


def read_sensors(name: Union[str, Path]) -> pl.DataFrame:
    return pl.read_csv(name).with_columns([
        col('sensor_id').cast(pl.Int16),
        col('x').cast(pl.Float32),
        col('y').cast(pl.Float32),
        col('z').cast(pl.Float32),
    ])


def join_sensors(df: pl.DataFrame, sensors: pl.DataFrame) -> pl.DataFrame:
    return df.join(sensors, on='sensor_id').drop('sensor_id')


class Mode(Enum):
    train = 'train'
    test = 'test'


def sample_generator(
        base_path: Union[str, Path],
        part_id: int,
        seq_len: int,
        sensors: Optional[pl.DataFrame] = None,
        mode: Mode = Mode.train
) -> Generator[List[Tuple[float, float, float, float, float, float]], None, None]:
    base_path = Path(base_path)
    meta = read_meta(base_path / f'{mode.value}_meta.parquet',
                     part_id,
                     columns=['event_id', 'azimuth', 'zenith'] if mode.train else ['event_id'],
                     cartesian_coords=mode is Mode.train)

    sensors = sensors if sensors is not None else read_sensors(base_path / 'sensor_geometry.csv')

    part = read_part(base_path / mode.value / f'batch_{part_id}.parquet')
    part = part.with_columns(
        # pl.Series([0]).append(
        #     part['time'][1:] - part['time'][:-1]
        # ).cast(pl.Float32).alias('time')
        col('time').cast(pl.Float32) / 1e9
    )
    part = join_sensors(part, sensors)
    part = part.join(meta, on='event_id')
    if mode is Mode.train:
        part = part.select([
            'event_id', 'time', 'charge', 'auxiliary', 'x', 'y', 'z', 'tx', 'ty', 'tz'
        ])
    else:
        part = part.select([
            'event_id', 'time', 'charge', 'auxiliary', 'x', 'y', 'z'
        ])

    last_event = -1
    seq = []
    target = None
    for tup in part.iter_rows(named=False):
        if mode is Mode.train:
            event_id, time, charge, aux, x, y, z, tx, ty, tz = tup
        else:
            event_id, time, charge, aux, x, y, z = tup

        if last_event != event_id and last_event > 0:
            seq.extend([PAD] * (seq_len - len(seq)))

        if len(seq) == seq_len:
            yield seq, target
            seq = []

        seq.append((time, charge, aux, x, y, z))
        target = (event_id, tx, ty, tz) if mode is Mode.train else (event_id,)
        last_event = event_id

    if seq:
        seq.extend([PAD] * (seq_len - len(seq)))
        yield seq, target


class IceCubeDataset(IterableDataset):

    def __init__(self, base_path: str | Path, sequence_length: int = 256, mode: Mode = Mode.train):
        self.base_path = Path(base_path)
        self.mode = mode
        self.seq_len = max(16, sequence_length or 0)

    def get_part_ids(self):
        if self.mode is Mode.train:
            if info := get_worker_info():
                _id = info.id
                n = info.num_workers
                m = MAX_TRAIN_BATCH_ID // n
            else:
                _id = 0
                n = 1
                m = 1

            part_ids = list(range(1, MAX_TRAIN_BATCH_ID + 1))

            if _id != n - 1:
                parts = part_ids[m * _id: m * (_id + 1)]
            else:
                parts = part_ids[m * _id:]

            log.info(f'Worker {_id}/{n - 1}: targeted {parts=}')
            return parts
        else:
            assert get_worker_info() is None, "Multiprocessing is not allowed while testing"
            parts = [MAX_TEST_BATCH_ID]
            log.info(f'Main worker: testing mode - targeted {parts=}')
            return parts

    def __iter__(self):
        sensors = read_sensors(self.base_path / 'sensor_geometry.csv')
        for p in self.get_part_ids():
            yield from sample_generator(
                base_path=self.base_path,
                part_id=p,
                seq_len=self.seq_len,
                mode=self.mode,
                sensors=sensors
            )


def collate_fn(batch: List[Tuple[list, tuple]]):
    return FloatTensor([x for x, _ in batch]), FloatTensor([y for _, y in batch])


def create_dataloader(
        base_path: str | Path,
        sequence_length: int = 256,
        batch_size: int = 2048,
        mode: Mode = Mode.train,
        num_workers: Optional[int] = None
) -> DataLoader:
    base_path = Path(base_path)
    log.info(f'Each batch weights {batch_size * sequence_length * 6 * 4 * 8 / 1e6:.3f}Mb')
    ds = IceCubeDataset(base_path, sequence_length=sequence_length, mode=mode)
    dl = DataLoader(
        ds,
        batch_size=batch_size,
        num_workers=(num_workers or 0) if mode is not Mode.test else 0,
        pin_memory=True,
        collate_fn=collate_fn,
        drop_last=mode is not Mode.test,
    )

    def __len__(self):
        if mode is mode.train:
            end = pl.read_parquet('dataset/train_meta.parquet', columns=['last_pulse_index']).get_columns()[0]
        elif mode is mode.test:
            end = pl.read_parquet('dataset/test_meta.parquet', columns=['last_pulse_index']).get_columns()[0]
        else:
            raise
        end = (end[:1] + 1).append((end[1:] - end[:-1]))
        length = end // sequence_length
        length += (end % sequence_length > 0).cast(int)
        return length.sum() // batch_size

    dl.__class__.__len__ = __len__
    return dl


__all__ = (
    'create_dataloader',
    'Mode'
)
