# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 10/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from collections import deque
from dataclasses import dataclass
from typing import Optional, Generator

import numpy as np
import torch
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm

from icecube.nn.utils import model_size

DEVICE = 'cpu'


@dataclass
class TrainHyperParameters:
    lr: float = 1
    lr_scheduler: bool = True


def train(
        model: nn.Module,
        dataloader: DataLoader,
        tr_hyp: TrainHyperParameters = TrainHyperParameters(),
        device: Optional[str] = None,
        **model_kwargs
) -> nn.Module:
    device = device or DEVICE

    t, n = model_size(model)
    print(
        f'Training model {model.name if hasattr(model, "name") else model.__class__.__name__}: '
        f'trainable parameters {t:,} / total parameters {n:,}'
    )
    model.to(device)
    model.train()
    optimizer = torch.optim.Adam(model.parameters(), lr=tr_hyp.lr)
    if tr_hyp.lr_scheduler:
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=50_000, gamma=0.95)
    else:
        class _NullScheduler:
            step = staticmethod(lambda: None)
            get_last_lr = staticmethod(lambda: [optimizer.state_dict()['param_groups'][0]['lr']])

        scheduler = _NullScheduler()

    total_loss = deque(maxlen=100)
    desc = 'Train: lr={:9.6f}, loss={:9.6f}'
    try:
        total = len(dataloader)
    except:
        total = None

    with tqdm(total=total, desc=desc.format(scheduler.get_last_lr()[0], float('inf'))) as pbar:
        for i, args in enumerate(dataloader):
            if isinstance(args, (tuple, list)):
                args = tuple(a.to(device, non_blocking=True) for a in args)
            else:
                args = (args.to(device, non_blocking=True),)

            loss: torch.Tensor = model.loss(*args, **model_kwargs)
            optimizer.zero_grad()
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
            optimizer.step()
            scheduler.step()

            total_loss.append(loss.item())

            pbar.update(1)
            pbar.set_description(desc.format(scheduler.get_last_lr()[0], sum(total_loss) / len(total_loss)))
            # TODO: model checkpointing

    return model


def infer(
        model: nn.Module,
        dataloader: DataLoader,
        device: Optional[str] = None,
        **model_kwargs
) -> Generator[np.ndarray, None, None]:
    device = getattr(dataloader, 'device', None) or device or DEVICE
    model.to(device)
    model.eval()

    with tqdm(total=len(dataloader), desc='Infer') as pbar:
        with torch.no_grad():
            for args in dataloader:
                if isinstance(args, (tuple, list)):
                    args = tuple(a.to(device, non_blocking=True) for a in args)
                else:
                    args = (args.to(device, non_blocking=True),)

                batch = model(*args, **model_kwargs)

                if isinstance(batch, tuple):
                    batch = tuple(b.cpu().numpy() for b in batch)
                else:
                    batch = batch.cpu().numpy()

                yield batch

                pbar.update(1)
