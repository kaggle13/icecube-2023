import logging

logging.basicConfig(level=logging.DEBUG)

__all__ = 'get_logger',


def get_logger(name):
    log = logging.getLogger(name)
    return log
