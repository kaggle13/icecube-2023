# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 10/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
def model_size(model) -> (int, int):
    counts = [(p.numel(), p.requires_grad) for p in model.parameters()]
    return sum(x for x, grad in counts if grad), sum(x for x, _ in counts)
