# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 10/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from dataclasses import dataclass

import torch
from torch import nn


@dataclass
class SimpleTransformerRegressorConfig:
    d_model: int = 512
    dim_feedforward: int = 512
    nhead: int = 8
    num_layers: int = 6


class SimpleTransformerRegressor(nn.Module):
    name = 'SimpleTransformerRegressor'

    def __init__(self, config: SimpleTransformerRegressorConfig):
        super(SimpleTransformerRegressor, self).__init__()

        self.bn = nn.BatchNorm1d(num_features=6)
        self.lin = nn.Linear(in_features=6, out_features=config.d_model)
        encoder_layer = nn.TransformerEncoderLayer(
            d_model=config.d_model,
            dim_feedforward=config.dim_feedforward,
            nhead=config.nhead,
            batch_first=True
        )
        self.regressor = nn.TransformerEncoder(encoder_layer, num_layers=config.num_layers)
        self.head = nn.Linear(in_features=config.d_model, out_features=3)

    def forward(self, x, target=None):
        x = torch.transpose(x, 1, 2)
        x = self.bn(x)
        x = torch.transpose(x, 1, 2)
        x = self.lin(x)
        x = self.regressor(x)
        x = self.head(x)
        out = x[:, 0, ...].squeeze()  # torch.mean(x, dim=1)

        if target is not None:
            assert not self.training, "target reference is only for inference: it must contain the event_id"
            return torch.cat((target[:, 0], out), dim=1)

        return out

    def loss(self, x, target):
        x_hat = self(x)
        return nn.functional.mse_loss(x_hat, target[:, 1:])
