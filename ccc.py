from typing import Generator, List, Tuple, Optional, Union

import numpy as np
import pandas as pd
from torch import FloatTensor
from torch.utils.data import IterableDataset, get_worker_info, DataLoader
from pathlib import Path
from enum import Enum
from icecube.constants import *


def read_meta(
        name: Union[str, Path], part_id: int,
        columns: List[str] = ('event_id', 'azimuth', 'zenith'),
        low_memory: bool = True,
        cartesian_coords: bool = True,
) -> pd.DataFrame:
    df = pd.read_parquet(name, columns=columns)
    df = df[df['batch_id'] == part_id]

    columns = [c for c in columns if c in df.columns]
    if columns:
        df = df[columns]

    if all(x in df.columns for x in ['azimuth', 'zenith']):
        df = df.astype({'azimuth': np.float32, 'zenith': np.float32})

        if cartesian_coords:
            df['tx'] = (df['azimuth'].apply(np.cos) * df['zenith'].apply(np.sin)).astype(np.float32)
            df['ty'] = (df['azimuth'].apply(np.sin) * df['zenith'].apply(np.sin)).astype(np.float32)
            df['tx'] = (df['zenith'].apply(np.cos)).astype(np.float32)
            df = df.drop(columns=['azimuth', 'zenith'])

    return df


def read_part(name: Union[str, Path]) -> pd.DataFrame:
    df = pd.read_parquet(name)
    df['charge'] = df['charge'].apply(np.log10)
    df = df.astype({
        'charge': np.float32,
        'sensor_id': np.int16,
        'auxiliary': np.float32,
    })
    return df


def read_sensors(name: Union[str, Path]) -> pd.DataFrame:
    return pd.read_csv(name).astype({
        'sensor_id': np.int16,
        'x': np.float32,
        'y': np.float32,
        'z': np.float32,
    })


def join_sensors(df: pd.DataFrame, sensors: pd.DataFrame) -> pd.DataFrame:
    return df.merge(sensors, on='sensor_id').drop(columns=['sensor_id'])


class Mode(Enum):
    train = 'train'
    test = 'test'


def sample_generator(
        base_path: Union[str, Path],
        part_id: int,
        seq_len: int,
        sensors: Optional[pd.DataFrame] = None,
        mode: Mode = Mode.train
) -> Generator[List[Tuple[float, float, float, float, float, float]], None, None]:
    base_path = Path(base_path)
    meta = read_meta(base_path / f'{mode.value}_meta.parquet',
                     part_id,
                     columns=['event_id', 'azimuth', 'zenith'] if mode.train else ['event_id'],
                     cartesian_coords=mode is Mode.train)

    sensors = sensors if sensors is not None else read_sensors(base_path / 'sensor_geometry.csv')

    part = read_part(base_path / mode.value / f'batch_{part_id}.parquet')
    part['time'] = (part['time'] / 1e9).astype(np.float32)
    part = join_sensors(part, sensors)
    part = part.merge(meta, on='event_id')
    if mode is Mode.train:
        part = part[[
            'event_id', 'time', 'charge', 'auxiliary', 'x', 'y', 'z', 'tx', 'ty', 'tz'
        ]]
    else:
        part = part[[
            'event_id', 'time', 'charge', 'auxiliary', 'x', 'y', 'z'
        ]]

    last_event = -1
    seq = []
    target = None
    for tup in part.itertuples(index=False, name=None):
        if mode is Mode.train:
            event_id, time, charge, aux, x, y, z, tx, ty, tz = tup
        else:
            event_id, time, charge, aux, x, y, z = tup

        if last_event != event_id and last_event > 0:
            seq.extend([PAD] * (seq_len - len(seq)))

        if len(seq) == seq_len:
            yield seq, target
            seq = []

        seq.append((time, charge, aux, x, y, z))
        target = (event_id, tx, ty, tz) if mode is Mode.train else (event_id,)
        last_event = event_id

    if seq:
        seq.extend([PAD] * (seq_len - len(seq)))
        yield seq, target


class IceCubeDataset(IterableDataset):

    def __init__(self, base_path: Union[str, Path], sequence_length: int = 256, mode: Mode = Mode.train):
        self.base_path = Path(base_path)
        self.mode = mode
        self.seq_len = max(16, sequence_length or 0)

    def get_part_ids(self):
        if self.mode is Mode.train:
            info = get_worker_info()
            if info:
                _id = info.id
                n = info.num_workers
                m = MAX_TRAIN_BATCH_ID // n
            else:
                _id = 0
                n = 1
                m = 1

            part_ids = list(range(1, MAX_TRAIN_BATCH_ID + 1))

            if _id != n - 1:
                parts = part_ids[m * _id: m * (_id + 1)]
            else:
                parts = part_ids[m * _id:]


            return parts
        else:
            assert get_worker_info() is None, "Multiprocessing is not allowed while testing"
            parts = [MAX_TEST_BATCH_ID]

            return parts

    def __iter__(self):
        sensors = read_sensors(self.base_path / 'sensor_geometry.csv')
        for p in self.get_part_ids():
            yield from sample_generator(
                base_path=self.base_path,
                part_id=p,
                seq_len=self.seq_len,
                mode=self.mode,
                sensors=sensors
            )


def collate_fn(batch: List[Tuple[list, tuple]]):
    return FloatTensor([x for x, _ in batch]), FloatTensor([y for _, y in batch])


def create_dataloader(
        base_path: Union[str, Path],
        sequence_length: int = 256,
        batch_size: int = 2048,
        mode: Mode = Mode.train,
        num_workers: Optional[int] = None
) -> DataLoader:
    base_path = Path(base_path)

    ds = IceCubeDataset(base_path, sequence_length=sequence_length, mode=mode)
    dl = DataLoader(
        ds,
        batch_size=batch_size,
        num_workers=(num_workers or 0) if mode is not Mode.test else 0,
        pin_memory=True,
        collate_fn=collate_fn,
        drop_last=mode is not Mode.test,
    )

    def __len__(self):
        if mode is Mode.train:
            end = pd.read_parquet(base_path / 'train_meta.parquet', columns=['last_pulse_index'])['last_pulse_index']
        elif mode is Mode.test:
            end = pd.read_parquet(base_path / 'test_meta.parquet', columns=['last_pulse_index'])['last_pulse_index']
        else:
            raise
        end = end.to_numpy()
        end = np.concatenate(([end[0] + 1], end[1:] - end[:-1]))
        length = end // sequence_length
        length += (end % sequence_length > 0).astype(int)
        return length.sum() // batch_size

    dl.__class__.__len__ = __len__
    return dl