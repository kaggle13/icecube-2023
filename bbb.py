import pandas as pd

df = pd.read_parquet('dataset/train_meta.parquet')
for i in range(1, 661):
    df[df['batch_id'] == i].drop(columns=['batch_id']).to_parquet(f'dataset/train/batch_{i}_meta.parquet')
    print(i)
