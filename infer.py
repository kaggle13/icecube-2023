# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 10/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
from pathlib import Path
from typing import Any, Union

import numpy as np
import polars as pl
import torch
from polars import col

from icecube.dataloaders.sequences import create_dataloader, Mode
from icecube.loops import infer


def find_model_version(model_name: str, path: Path) -> int:
    import re
    path = Path(path)
    vers = int(
        re.search(f'{model_name}\\.(\\d+)', str(max(list(path.glob(f'{model_name}.*.pt'))))).group(1)
    )
    vers += 1
    return vers


def load(path: Union[str, Path]) -> (Any, torch.nn.Module, float):
    x = torch.load(path)
    model: torch.nn.Module = x['model_class'](x['cfg'])
    model.load_state_dict(x['model_state_dict'])
    return x['cfg'], model, x['val_loss']


if __name__ == '__main__':
    cgf, model, _ = load('artifacts')

    dl = create_dataloader(base_path='dataset', num_workers=2, batch_size=64, sequence_length=256, mode=Mode.test)

    outputs = [batch for batch in infer(model, dl, device='cuda' if torch.cuda.is_available() else 'cpu')]
    outputs = np.concatenate(outputs)
    outputs = pl.DataFrame({
        'event_id': outputs[:, 0],
        'x': outputs[:, 1],
        'y': outputs[:, 2],
        'z': outputs[:, 3],
    }).with_columns([col('event_id').cast(pl.Int64)])
    outputs = outputs.groupby('event_id').agg(pl.mean(['x', 'y', 'z'])).with_columns([
        (col('y') / col('x')).apply(np.arctan).alias('azimuth'),
        (
                (col('x') ** 2 + col('y') ** 2).apply(np.sqrt) / col('z')
        ).apply(np.arctan).alias('zenith'),
    ]).drop(['x', 'y', 'z'])
    outputs.write_csv('artifacts/submission.csv')
