# ---------------------------------------------------------
# Author: Ludovico Frizziero - ludovico.frizziero@gmail.com
# Date: 10/02/2023
# copyright: Ludovico Frizziero
# ---------------------------------------------------------
import re
from pathlib import Path

import torch

from icecube.dataloaders.sequences import create_dataloader
from icecube.loops import train, TrainHyperParameters
from icecube.nn.modules import SimpleTransformerRegressor, SimpleTransformerRegressorConfig

if __name__ == '__main__':
    cfg = SimpleTransformerRegressorConfig(
        d_model=256,
        dim_feedforward=1024,
        nhead=8,
        num_layers=4,
    )
    model = SimpleTransformerRegressor(cfg)

    dl = create_dataloader(base_path='dataset', num_workers=2, batch_size=64, sequence_length=256)

    try:
        model = train(model, dl, TrainHyperParameters(lr=0.001), device='cuda')
    except KeyboardInterrupt:
        print('CTRL-C: stopped by user.')
    finally:
        vers = int(
            re.search(f'{model.name}\\.(\\d+)', str(max(list(Path('artifacts').glob(f'{model.name}.*.pt'))))).group(1)
        )
        vers += 1
        torch.save(
            {
                'cfg': cfg,
                'model_state_dict': model.state_dict(),
                'model_class': model.__class__,
                'val_loss': float('inf')
            },
            f'artifacts/{model.name}.{vers:03d}.pt'
        )
